from  rest_framework import  serializers
from api.models import Message
from api.utils import get_client_ip


class MessageSerializer(serializers.ModelSerializer):
    ip_address = serializers.IPAddressField(read_only=True)
    class Meta:
        model = Message
        fields = ["id","created_at","text","ip_address"]

    
    def create(self,validated_data):
        ip_address = get_client_ip(self.context['request'])
        message = Message.objects.create(**validated_data,ip_address=ip_address)
        
        return message
    

