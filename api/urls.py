from django.contrib import admin
from django.urls import path,include
from api.views import MessageListCreateAPIView,delete_message,delete_all_messages

urlpatterns = [
    path('messages',MessageListCreateAPIView.as_view()),
    path('messages/delete/<pk>',delete_message),
    path('messages/delete-all',delete_all_messages)
]
