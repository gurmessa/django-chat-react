from django.shortcuts import get_object_or_404, render
from django.conf import settings

from rest_framework import generics
from rest_framework.decorators import api_view
from rest_framework.response import Response
from rest_framework import status 


from api.models import Message
from api.serializers import MessageSerializer

class MessageListCreateAPIView(generics.ListCreateAPIView):
    queryset = Message.objects.all()
    serializer_class = MessageSerializer


@api_view(['DELETE'])
def delete_message(request,pk):
    is_valid , response = validate_secret_key(request)
    if not is_valid:
        return response 

    message = get_object_or_404(Message,pk=pk)
    message.delete()

    return Response({},status=status.HTTP_204_NO_CONTENT)


@api_view(['DELETE'])
def delete_all_messages(request):
    is_valid , response = validate_secret_key(request)
    if not is_valid:
        return response 
    Message.objects.all().delete()

    return Response({},status=status.HTTP_204_NO_CONTENT)



def validate_secret_key(request):
    """ 
    returns is_valid boolean status and Response(if invalid) 
    """
    if 'HTTP_AUTHORIZATION' not in request.META:
        return False,Response({"message":"Token is required"},status=status.HTTP_401_UNAUTHORIZED)
    else:
        if request.META['HTTP_AUTHORIZATION'] != settings.SECRET_TOKEN:
            return False,Response({"message":"Invalid Token"},status=status.HTTP_401_UNAUTHORIZED)
    return True,None