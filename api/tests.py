from django.test import TestCase
from django.core.exceptions import ValidationError
from django.conf import settings

from rest_framework.test import APIClient 
from rest_framework import status 

from api.models import Message


class MessageModelTest(TestCase):
    def test_saving_messages(self):
    
        message = Message()
        message.text = "Hello World"
        message.ip_address = "192.168.1.1"
        message.save()

        saved_message = Message.objects.last()

        self.assertEqual(message,saved_message)
        self.assertEqual(saved_message.text,"Hello World")
        self.assertNotEqual(saved_message.created_at,None)

    
class TestListCreateMessage(TestCase):
    def setUp(self):
        self.client = APIClient()

    def test_create_message(self):
        data = {"text":"Hello world"}
        response = self.client.post('/api/messages',data,format='json')
        message = Message.objects.last()

        self.assertEqual(response.status_code,status.HTTP_201_CREATED)
        self.assertEqual(message.text,"Hello world")
        self.assertNotEqual(message.ip_address,None)
        self.assertNotEqual(message.ip_address,"")
        self.assertEqual(Message.objects.count(),1)

    
    def test_message_list(self):
        Message.objects.create(text="test1",ip_address="192.168.1.1")
        Message.objects.create(text="test2",ip_address="192.168.1.1")

        response = self.client.get('/api/messages',format='json')
        self.assertEqual(response.status_code,status.HTTP_200_OK)
        data = response.data
        self.assertEqual(len(data),2)


class TestDeleteMessage(TestCase):
    def test_delete_message(self):
        message = Message.objects.create(text="test",ip_address="192.168.1.1")
        
        client = APIClient()
        client.credentials(HTTP_AUTHORIZATION=settings.SECRET_TOKEN)

        response = client.delete(f'/api/messages/delete/{message.id}')
        self.assertEqual(response.status_code,status.HTTP_204_NO_CONTENT)
        self.assertEqual(Message.objects.count(),0)

    def test_invalid_token_can_not_delete(self):
        client = APIClient()
        client.credentials(HTTP_AUTHORIZATION="12345")
        message = Message.objects.create(text="test",ip_address="192.168.1.1")
        response = client.delete(f'/api/messages/delete/{message.id}')
        self.assertEqual(response.status_code,status.HTTP_401_UNAUTHORIZED)
        self.assertEqual(Message.objects.count(),1)

    def test_invalid_id_returns_404(self):
        client = APIClient()
        client.credentials(HTTP_AUTHORIZATION=settings.SECRET_TOKEN)
        response = client.delete('/api/messages/delete/50000')
        self.assertEqual(response.status_code,status.HTTP_404_NOT_FOUND)


    def test_missing_token_return_401(self):
        message = Message.objects.create(text="test",ip_address="192.168.1.1")
        client = APIClient()
        response = client.delete(f'/api/messages/delete/{message.id}')
        self.assertEqual(response.status_code,status.HTTP_401_UNAUTHORIZED)


class TestDeleteAll(TestCase):
    def test_delete_all(self):
        Message.objects.create(text="test",ip_address="192.168.1.1")
        Message.objects.create(text="test",ip_address="192.168.1.1")
        
        client = APIClient()
        client.credentials(HTTP_AUTHORIZATION=settings.SECRET_TOKEN)

        response = client.delete('/api/messages/delete-all')
        self.assertEqual(response.status_code,status.HTTP_204_NO_CONTENT)
        self.assertEqual(Message.objects.count(),0)
